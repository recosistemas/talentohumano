<?php

use Illuminate\Support\Facades\Route;



Route::get('/dashboard', function () {
    return view('template.dashboard');
});

Route::resource('empresas' ,'App\Http\Controllers\Gestion\EmpresasController');
Route::resource('bancos' ,'App\Http\Controllers\Gestion\BancosController');