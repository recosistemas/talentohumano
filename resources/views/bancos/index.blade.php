@extends('template.index')
@section('estilos')
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('title', 'Empresas')
@section('content')
<style>
  .btn {
  border: none;
  cursor: pointer;
}
.error{
  color: red;
}

</style>

<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Empresas</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">Empresas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
           
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-11"><h3 class="card-title">Listado de Empresas</h3> </div>
                  <div class="col-sm-1-right "><button class="btn btn-primary" id="btn-add" name="btn-add"><i class="fas fa-plus-square"> Nuevo</i></button></div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="empresas_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Codigo Empresa</th>
                    <th>Nombre</th>
                    <th>Razón Social</th>
                    <th>Ruc</th>
                    <th>Logo</th>
                    <th>estado</th>
                    <th>Fecha actualización</th>
                    <th>Usuario</th>
                    <th>Acciones</th>
                  </tr>
                  </thead>
                  <tbody id="empresas-list" name="empresas-list">
                  @foreach ($empresas as $empresa)
                  <tr id="empresa{{$empresa->id}}">
                    <th>{{$empresa->id}}</th>
                    <th>{{$empresa->codigo_empresa}}</th>
                    <th>{{$empresa->nombre}}</th>
                    <th>{{$empresa->razon_social}}</th>
                    <th>{{$empresa->ruc}}</th>
                    <th>logo</th>
                    <th>{{$empresa->estado}}</th>
                    <th>{{$empresa->updated_at}}</th>
                    <th>{{$empresa->usuario}}</th>
                    <th>
                    <div class="btn-group">
                      <button class="btn btn-primary open-modal" value="{{$empresa->id}}"><i class="fas fa-edit"></i></button>
                    </div>
                    </th>
                  </tr>
                  @endforeach
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 
</div>


<div class="modal fade" id="empresaModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                              <label for="" name="empresaModalLabel" id="empresaModalLabel"></label>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate="" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Código empresa:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="codigo_empresa" name="codigo_empresa" placeholder="Ingrese código de la empresa" tabindex ="1" value="">
                                        @error('codigo_empresa')
                                        <br >
                                          <small >{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nombre:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese nombre de la empresa" tabindex ="2" value="">
                                        @error('nombre')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Razon Social:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="razon_social" name="razon_social" placeholder="Ingrese razon social" tabindex ="3" value="">
                                        @error('razon_social')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ruc:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="ruc" name="ruc" placeholder="Ingrese Ruc o numero de cedula" tabindex ="4" value="">
                                        @error('ruc')
                                        <br>
                                          <small>{{message}}</small>
                                        <br>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Logo:</label>
                                    <div class="col-sm-10">
                                        <input type="file" id="logo" name="logo"  tabindex ="5" accept=".jpg, .jpeg, .png">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Estado:</label>
                                    <div class="col-sm-10">
                                      <select name="estado" id="estado"  tabindex ="6">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>
                                        <option value="E">Eliminado</option>
                                      </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" name="btn-save" value="add" tabindex ="7" disabled="true">Guardar
                            </button>
                            <input type="hidden" id="empresa_id" name="empresa_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>
  document.addEventListener('keyup', validateForm);

  function validateForm(e) {
    let validation = jQuery("#modalFormData").validate().errorList.length>0;
    //Agregar todos los inputs 
    let codigo_empresa = jQuery("#codigo_empresa").val();
    let name = jQuery("#nombre").val();
    let razon_social = jQuery("#razon_social").val();
    let ruc = jQuery("#ruc").val();


    if(name.length == 0 || razon_social.length == 0 || codigo_empresa.length == 0 || ruc.length == 0){
      $("#btn-save").prop('disabled', true);
    }else{
      $("#btn-save").prop('disabled', validation);
    }
    
  }

  
jQuery(function() {
  jQuery("#modalFormData").validate({
    rules: {
      codigo_empresa: { required: true,minlength: 3,maxlength: 3},
      nombre: { required: true, minlength: 1, maxlength: 250},
      razon_social: { required: true, minlength: 1, maxlength: 500},
      ruc: { required: true,minlength: 13,maxlength: 13,number: true},
      estado: { required: true}
    },
		messages: {
			ruc: {
              required: "Campo obligatorio",
              minlength: $.format("Necesitamos por lo menos 13 caracteres"),
              maxlength: $.format("{0} caracteres son demasiados!"),
              number: "Solo puede ingresar números"
            },
      nombre: { required: "Campo obligatorio"},
      razon_social: { required: "Campo obligatorio"},

    }
   });
});
$(function () {
    $("#empresas_table").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#empresas_table_wrapper .col-md-6:eq(0)');
});
  
  jQuery('body').on('click', '.open-modal', function () {
        var empresa_id = $(this).val();
        $.get('empresas/' + empresa_id+'/edit', function (data) {
          data.forEach(data => {
            console.log(data.id);
              jQuery('#empresa_id').val(data.id);
              jQuery('#codigo_empresa').val(data.codigo_empresa);
              jQuery('#nombre').val(data.nombre);
              jQuery('#razon_social').val(data.razon_social);
              jQuery('#ruc').val(data.ruc);
              jQuery('#estado').val(data.estado);
          });
              jQuery('#empresaModalLabel').text("Editar Empresa");
              jQuery('#btn-save').val("update");
              jQuery('#empresaModal').modal('show');
            
        })
    });
    
  jQuery('#btn-add').click(function () {
     jQuery('#btn-save').val("add");
     jQuery('#modalFormData').trigger("reset");
     jQuery('#empresaModalLabel').text("Nueva Empresa");
     jQuery('#empresaModal').modal('show');
     
 });
 $("#btn-save").click(function (e) {
    console.log(jQuery('#logo')[0].files[0]);
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': "{{ csrf_token() }}",
         }
     });
     e.preventDefault();
     var formData = {
         codigo_empresa: jQuery('#codigo_empresa').val(),
         nombre: jQuery('#nombre').val(),           
         razon_social: jQuery('#razon_social').val(),          
         ruc: jQuery('#ruc').val(),
         estado: jQuery('#estado').val(),
         
     };
     var state = jQuery('#btn-save').val();
     var type = "POST";
     var empresa_id = jQuery('#empresa_id').val();
     var ajaxurl = 'empresas';
     if (state == "update") {
         type = "PUT";
         ajaxurl = 'empresas/' + empresa_id;
         console.log(empresa_id);
     }
     $.ajax({
         type: type,
         url: ajaxurl,
         data: formData,
         dataType: 'json',         
         success: function (data) {
             console.log(state);
             console.log(data[0].id);
             console.log(data);
             var empresa;
             data.forEach(data => {
              empresa = '<tr id="'+ data.id+'"> <td>'+data.id+'</td>  <td> ' +data.codigo_empresa+ ' </td> <td> ' + data.nombre + ' </td>  <td> '+data.razon_social +'</td> <td> ' + data.ruc + ' </td>  <td> '+'LOGO' + ' </td>  <td> '+data.estado +' </td>  <td> '+data.updated_at +'</td> <td> '+data.usuario  +'</td>';
              empresa += '<td> <button class="btn btn-info open-modal" value="' + data.id + '"><i class="fas fa-edit"></i></button>';
             });
              if (state == "add") {
                 jQuery('#empresas-list').append(empresa);
             } else {
                 $("#empresa" + empresa_id).replaceWith(empresa);
             }
             jQuery('#modalFormData').trigger("reset");
             jQuery('#empresaModal').modal('hide')
         },
         error: function (data) {
             console.log('Error:', data);
         }
     });
    
  

 });
 

</script>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

@endsection


 



