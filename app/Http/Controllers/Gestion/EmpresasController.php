<?php

namespace App\Http\Controllers\Gestion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empresa;
use DB;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = DB::select("sp_web_empresas_consultar");
        //dd($empresas);
        return view('empresas.index')->with('empresas',$empresas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump ($request);
        /*$validator = Validator::make($request->all(), [
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);
          $input = $request->all();*/
        //dd($request->file('logo'));
        
       // dd($request->get('logo')->storeAs('uploads', '$fileName', 'public'));
        $usuario = auth()->user()->id;
        $empresas = DB::select("exec sp_web_empresas_insertar ?,?,?,?,?,?,?,?" ,
        array($request->get('codigo_empresa'),
        $request->get('nombre'),
        $request->get('razon_social'),
        $request->get('ruc'),
        'a','a',
        $request->get('estado'),
        $usuario ));
        return Response()->json($empresas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = DB::select("exec sp_web_empresas_editar ?" ,
        array($id ));
        return Response()->json($empresa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = auth()->user()->id;
        $empresas = DB::select("exec sp_web_empresas_actualizar ?,?,?,?,?,?,?,?,?" ,
        array($id,
        $request->get('codigo_empresa'),
        $request->get('nombre'),
        $request->get('razon_social'),
        $request->get('ruc'),
        'a','c',
        $request->get('estado'),
        $usuario ));
        return Response()->json($empresas);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
